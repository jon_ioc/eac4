package cat.xtec.ioc.eac4;

import java.util.Scanner;

/**
 * He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!
 */

/**
 * Calcula el factorial de un número solicitado por pantalla
 * 
 * @author Jon Mugarza
 * @version 1.0.0
 */
public class GitFactorial {
        
        /**
         * Método main 
         * 
         * @param args 
         */
	public static void main(String[] args) {
            
            // Crea entrada de teclado e ignora advertencias de recursos
            @SuppressWarnings("resource")
                    Scanner scanner = new Scanner(System.in);
            
            // Solicita un número entero por teclado
            System.out.print("Introdueix un nombre per calcular el factorial: ");
            int num = scanner.nextInt();

            // Solicita el tipo de calculo del factorial: recursivo o sin recursividad
            System.out.print("Escull la forma de càlcul (1 per recursiva, 2 sense recursivitat): ");
            int opcio = scanner.nextInt();

            /** 
             * Opción 1 realiza calculo recursivo de factorial de num
             * Opción 2 realiza calculo no recursivo de factorial de num
             * Otras opciones: mensaje de error y return
             */
            int resultat;

            switch (opcio) {
                case 1:
                    resultat = factorialRecursiu(num);
                    break;
                case 2:
                    resultat = factorialNoRecursiu(num);
                    break;
                default:
                    System.out.println("Opció no vàlida. Si us plau, selecciona 1 o 2.");
                    return;
            }
            // Muestra resultado de cálculo por pantalla
            System.out.println("El factorial de " + num + " és: " + resultat);

	}
	
        
        /**
         * Calcula el factorial de "n" de forma recursiva
         * 
         * @param n - un número entero
         * @return factorial de n
         * @throws Error
         */
	public static int factorialRecursiu(int n) {
            
            // Si "n" es menor que 0, muestra error
            if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
            // Si "n" es 1 o 0, el factorial es 1
            if (n == 0 || n == 1)
                return 1;
            // Llama a misma función hasta que "n" sea 1
            else
                return n * factorialRecursiu(n - 1);
        }
	
        /**
         * Calcula el factorial de "n" de forma iterativa
         * 
         * @param n - un número entero
         * @return resultat - factorial de n
         * @throws Error
         */
	public static int factorialNoRecursiu(int n) {
            
            // Si "n" es menor que 0, muestra error
            if (n < 0) {
	        throw new Error("El nombre no pot ser negatiu");
	    }
            /* Multiplica el resultado de multiplicar cada número por el siguiente
             * desde 1 hasta "n" 
             */
            int resultat = 1;
            for (int i = 1; i <= n; i++) {
                resultat *= i;
            }
            return resultat;
        }

}